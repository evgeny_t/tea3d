#include "hotpointwidget.h"
#include "ui_hotpointwidget.h"

HotpointWidget::HotpointWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::HotpointWidget)
{
    ui->setupUi(this);

    connect(ui->spinboxBias, SIGNAL(valueChanged(double)), this, SLOT(setBias(double)));
    connect(ui->spinboxTension, SIGNAL(valueChanged(double)), this, SLOT(setTension(double)));
    connect(ui->spinboxContinuity, SIGNAL(valueChanged(double)), this, SLOT(setContinuity(double)));
}

HotpointWidget::~HotpointWidget()
{
    delete ui;
}

void HotpointWidget::setAttributes(const QSet<quint32> &ss, DocumentPtr doc)
{
    if (!(doc && ss.size()))
    {
        parentWidget()->hide();
        return;
    }

    doc_ = doc;
    auto id = *ss.begin();
    auto item = doc->item(id);
    if (item)
    {
        auto hp = item.dynamicCast<Hotpoint>();
        if (hp)
        {
            model_ = hp;
            ui->spinboxBias->setValue(hp->bias());
            ui->spinboxContinuity->setValue(hp->continuity());
            ui->spinboxTension->setValue(hp->tension());

            parentWidget()->show();
        }
    }
}

void HotpointWidget::setBias(double v)
{
    auto hp = model_.dynamicCast<Hotpoint>();
    if (hp)
    {
        hp->setBias(v);
        emit changed();
    }
}

void HotpointWidget::setTension(double v)
{
    auto hp = model_.dynamicCast<Hotpoint>();
    if (hp)
    {
        hp->setTension(v);
        emit changed();
    }
}

void HotpointWidget::setContinuity(double v)
{
    auto hp = model_.dynamicCast<Hotpoint>();
    if (hp)
    {
        hp->setContinuity(v);
        emit changed();
    }
}
