#include "application.h"

#include <QFileDialog>
#include <QMessageBox>

Application::Application(QApplication *app) :
    QObject(app),
    m_debug(false)
{
    auto doc = DocumentPtr::create();
    current_ = doc;

    if (app->arguments().contains("--debug"))
        setDebug(true);
}

bool Application::tryCloseCurrent()
{
    if (current_ && current_->dirty())
    {
#ifdef QT_DEBUG
        return true;
#else
        DocumentPtr current = current_;
        QString message("The document %1 has been modified. \
Do you want to save your changes or discard them?");
        auto result = QMessageBox::question(
                          nullptr, "Close document", message.arg(current->filename()),
                          QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
        if (result == QMessageBox::Cancel)
        {
            return false;
        }
        else if (result == QMessageBox::Save)
        {
            return saveCurrent();
        }
#endif
    }

    return true;
}

bool Application::debug() const
{
    return m_debug;
}

void Application::newDocument()
{
    if (tryCloseCurrent())
    {
        current_ = DocumentPtr::create();
        emit opened(current_.toWeakRef());
    }
}

bool Application::saveCurrent()
{
    if (current_)
    {
        DocumentPtr current = current_;
        if (QFile::exists(current->filename()))
        {
            current->save();
        }
        else
        {
            QString filename = QFileDialog::getSaveFileName(
                                nullptr, "Save Spline File", current->filename(), Filter);
            if (!filename.isEmpty())
            {
                bool ok = current->save(filename);
                qDebug() << "Application::saveCurrent(): " << ok;
            }
            else
            {
                return false;
            }
        }

        return true;
    }

    return false;
}

void Application::open()
{
    if (!tryCloseCurrent())
    {
        return;
    }

    QString filename = QFileDialog::getOpenFileName(
                           nullptr, "Open Splines File", "", Filter);
    if (filename.isNull() || filename.isEmpty())
    {
        return;
    }

    auto loaded = Document::load(filename);
    if (loaded)
    {
        current_ = loaded;
        emit opened(loaded.toWeakRef());
    }
}

void Application::setDebug(bool debug)
{
    if (m_debug == debug)
        return;

    m_debug = debug;
    emit debugChanged(debug);
}
