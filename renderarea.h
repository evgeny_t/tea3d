#ifndef RENDERAREA_H
#define RENDERAREA_H

#include "irenderarea.h"

#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <QOpenGLShaderProgram>
#include <QMatrix4x4>
#include <QSet>

#include <QtDebug>

#include "document.h"

#include "spline3djig.h"

#include "spline3d.h"
#include "frustum.h"
#include "reference.h"

class RenderArea : public QOpenGLWidget, protected IRenderArea
{
    Q_OBJECT
public:
    explicit RenderArea(QWidget *parent = 0);
    virtual ~RenderArea();

    // --- IRenderArea impl ---
    virtual void displace(const QMatrix4x4 &) override;
    virtual void setObjectColor(const QVector3D &c) override;
    virtual void drawObject(Drawable*) override;
    virtual const QSet<quint32> &selectionset() override;
    virtual QVector3D cursorToWorld(int, const QPoint &screenPoint) override;
    virtual QVector3D cursorToWorld(const QMatrix4x4 &local, const QPoint &screenPoint) override;
    // ---

signals:
    void ready();
    void rendered(QImage);
    void selected(const QSet<quint32> &ss, DocumentPtr);
public slots:
    void setDocument(DocumentPtr);
    void startJig();
protected:
    virtual void initializeGL() override;
    virtual void resizeGL(int, int) override;
    virtual void paintGL() override;

    virtual void mousePressEvent(QMouseEvent *) override;
    virtual void mouseReleaseEvent(QMouseEvent *) override;
    virtual void mouseDoubleClickEvent(QMouseEvent *) override;
    virtual void mouseMoveEvent(QMouseEvent *) override;
    virtual void wheelEvent(QWheelEvent *) override;
    virtual void keyPressEvent(QKeyEvent *) override;

private:
    QVector3D cursorToWorld(const QPoint &);
    void drawScene();
    void cleanUp();

    QMatrix4x4 projection_;
    QMatrix4x4 model_;
    QMatrix4x4 model_2;
    QVector<DrawablePtr> objects_;
    QOpenGLShaderProgram sp_;
    QPoint cursor_;
    QSet<quint32> highlightset_;
    QSet<quint32> selectionset_;

    bool selectionMode_;
    bool drag_ = false;

    DocumentPtr document_;
};

#endif // RENDERAREA_H
