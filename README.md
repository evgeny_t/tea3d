# README #


### TODO ###

* Issue: `Drawable` can only be created when there is an active ogl context
* Issue: `Document` contains `Drawable`s. Document is not actually a document
* Issue: `Reference`s and `Hotpoint`s scale when user is zooming in/out
* Issue: hardcoded colors and other visual attribs
* Issue: `Hotpoint` is Spline3d-specific
* Issue: don't rebuild whole spline when one knot gets changed
* Issue: attributes panel on the right is `Hotpoint`s-aware

* Feature: undo/redo, implement Command pattern
* Feature: implement [Arcball](https://en.wikibooks.org/wiki/OpenGL_Programming/Modern_OpenGL_Tutorial_Arcball)
* Feature: draw a XOZ plane
* Feature: capability to insert a new spline knot at an arbitrary place