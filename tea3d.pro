#-------------------------------------------------
#
# Project created by QtCreator 2016-01-03T23:11:13
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = tea3d
TEMPLATE = app

CONFIG += c++11

QTPLUGIN += svg

SOURCES += main.cpp\
        mainwindow.cpp \
    renderarea.cpp \
    spline3d.cpp \
    debugarea.cpp \
    drawable.cpp \
    frustum.cpp \
    reference.cpp \
    irenderarea.cpp \
    document.cpp \
    spline3djig.cpp \
    attributeswidget.cpp \
    hotpoint.cpp \
    hotpointwidget.cpp \
    application.cpp

HEADERS  += mainwindow.h \
    renderarea.h \
    spline3d.h \
    debugarea.h \
    drawable.h \
    frustum.h \
    reference.h \
    irenderarea.h \
    document.h \
    spline3djig.h \
    attributeswidget.h \
    hotpoint.h \
    hotpointwidget.h \
    application.h

RESOURCES += \
    tea3d.qrc

DISTFILES += \
    main.frag \
    main.vert \
    README.md \
    tea3d.sh

FORMS += \
    hotpointwidget.ui

unix {
    message("unix build $$[QT_INSTALL_LIBS]")
    bin.path = $$OUT_PWD
    bin.files = \
        $$PWD\tea3d.sh \
        $$[QT_INSTALL_LIBS]/libQt5Widgets.so.5 \
        $$[QT_INSTALL_LIBS]/libQt5Gui.so.5 \
        $$[QT_INSTALL_LIBS]/libQt5Core.so.5 \
        $$[QT_INSTALL_LIBS]/libQt5DBus.so.5 \
        $$[QT_INSTALL_LIBS]/libQt5XcbQpa.so.5 \
        $$[QT_INSTALL_LIBS]/libicui18n.so.54 \
        $$[QT_INSTALL_LIBS]/libicuuc.so.54 \
        $$[QT_INSTALL_LIBS]/libicudata.so.54

    plugins.path = $$OUT_PWD/platforms
    plugins.files = \
        $$[QT_INSTALL_PLUGINS]/platforms/libqlinuxfb.so \
        $$[QT_INSTALL_PLUGINS]/platforms/libqminimal.so \
        $$[QT_INSTALL_PLUGINS]/platforms/libqxcb.so

    pack.path = $$OUT_PWD
    pack.commands = tar -cf tea3d-linux.tar *.so.1 *.so.5 *.so.6 *.so.54 tea3d tea3d.sh platforms

    INSTALLS += bin plugins pack
}

win32 {
    message("win32 build $$[QT_INSTALL_PLUGINS]")
    message("$$TARGET")
    bin.path = $$OUT_PWD/release
    bin.files = \
        $$[QT_INSTALL_BINS]/Qt5Widgets.dll \
        $$[QT_INSTALL_BINS]/Qt5Gui.dll \
        $$[QT_INSTALL_BINS]/Qt5Core.dll \
        $$[QT_INSTALL_BINS]/Qt5Svg.dll \
        $$[QT_INSTALL_BINS]/icuin54.dll \
        $$[QT_INSTALL_BINS]/icuuc54.dll \
        $$[QT_INSTALL_BINS]/icudt54.dll \
        $$[QT_INSTALL_BINS]/libEGL.dll

    plugins.path = $$OUT_PWD/release/plugins/platforms
    plugins.files = \
        $$[QT_INSTALL_PLUGINS]\platforms\qwindows.dll

    pack.path = $$OUT_PWD

    INSTALLS += bin plugins pack
}
