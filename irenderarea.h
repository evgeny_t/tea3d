#ifndef IRENDERAREA_H
#define IRENDERAREA_H

#include <QSet>
#include <QOpenGLFunctions>
#include <QMatrix4x4>
#include "drawable.h"

class IRenderArea : public QOpenGLFunctions
{
public:
    IRenderArea() { }
    virtual ~IRenderArea() { };

    virtual void displace(const QMatrix4x4 &) = 0;
    virtual void setObjectColor(const QVector3D &c) = 0;
    virtual void drawObject(Drawable*) = 0;
    virtual const QSet<quint32> &selectionset() = 0;
    virtual QVector3D cursorToWorld(int, const QPoint &screenPoint) = 0;
    virtual QVector3D cursorToWorld(const QMatrix4x4 &local, const QPoint &screenPoint) = 0;
};

#endif // IRENDERAREA_H
