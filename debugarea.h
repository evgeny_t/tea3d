#ifndef DEBUGAREA_H
#define DEBUGAREA_H

#include <QWidget>

class DebugArea : public QWidget
{
    Q_OBJECT
public:
    explicit DebugArea(QWidget *parent = 0);

signals:

public slots:
    void updateArea(QImage);
protected:
    virtual void paintEvent(QPaintEvent *) override;
private:
    QImage image_;
};

#endif // DEBUGAREA_H
