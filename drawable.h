#ifndef DRAWABLE_H
#define DRAWABLE_H

#include <QSharedPointer>
#include <QColor>
#include <QObject>
#include <QVector>
#include <QVector3D>
#include <QMatrix4x4>
#include <QOpenGLBuffer>
#include <QOpenGLVertexArrayObject>

class IRenderArea;
class QOpenGLFunctions;

#define DEFINE_OBJECT_PTR(x) \
    typedef QSharedPointer<x> x##Ptr

class Drawable : public QObject
{
    Q_OBJECT
public:
    explicit Drawable(QObject *parent = 0);
    virtual ~Drawable();

    virtual bool hasChild(quint32);
    virtual QSharedPointer<Drawable> child(quint32);
    virtual void removeChild(quint32);

    void draw(IRenderArea *);

    const QVector3D &color() { return color_; }
    void setColor(const QColor &);

    quint32 id() { return id_; }

    const QMatrix4x4 &placement() { return placement_; }
    void setPlacement(const QMatrix4x4 &p) { placement_ = p; }
signals:

public slots:

protected:
//    virtual void rebuild(QVector<QVector3D> &, QVector<GLuint> &);
    virtual void internalDraw(IRenderArea *);
    virtual void setupVertexAttributePointers(IRenderArea *);
    virtual void internalDrawChildren(IRenderArea *);

    QOpenGLBuffer vbo_;
    QOpenGLBuffer ebo_;
    QOpenGLVertexArrayObject vao_;

    QVector<QVector3D> vertices_;
    QVector<GLuint> indices_;
private:
    QVector3D color_;
    quint32 id_;
    QMatrix4x4 placement_;
};

DEFINE_OBJECT_PTR(Drawable);

#endif // DRAWABLE_H
