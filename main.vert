#version 330 core

layout (location = 0) in vec3 position;
layout (location = 1) in vec2 textcoord;

uniform mat4 projection;
uniform mat4 view;
uniform mat4 model;
uniform mat4 placement;

out vec2 tc;

void main()
{
    gl_Position = projection * view * model * placement *
            vec4(position.x, position.y, position.z, 1.0);
    tc = textcoord;
}

//attribute vec4 qt_Vertex;
////attribute vec4 qt_MultiTexCoord0;
////varying vec4 qt_TexCoord0;

//void main(void)
//{
//    gl_Position = qt_ModelViewProjectionMatrix * qt_Vertex;
////    qt_TexCoord0 = qt_MultiTexCoord0;
//}
