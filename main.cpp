#include "mainwindow.h"

#include <ctime>

#include <QApplication>
#include <QOpenGLWindow>

#include "application.h"

int main(int argc, char *argv[])
{
    qsrand(time(NULL));

    QApplication a(argc, argv);

    Application app(&a);
    MainWindow w(app);
    w.show();

    return a.exec();
}
