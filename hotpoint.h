#ifndef HOTPOINT_H
#define HOTPOINT_H

#include <QSharedPointer>

#include "drawable.h"
#include "frustum.h"
#include "spline3d.h"

class Hotpoint : public Frustum
{
public:
    Hotpoint(QWeakPointer<Spline3D>, int);

    float bias();
    float tension();
    float continuity();

    void setBias(float);
    void setTension(float);
    void setContinuity(float);

protected:
    virtual void internalDraw(IRenderArea *) override;

    QWeakPointer<Spline3D> spline_;
    int idx_;
};

DEFINE_OBJECT_PTR(Hotpoint);

#endif // HOTPOINT_H
