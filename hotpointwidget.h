#ifndef HOTPOINTWIDGET_H
#define HOTPOINTWIDGET_H

#include <QSet>
#include <QWidget>

#include "document.h"
#include "drawable.h"

namespace Ui {
class HotpointWidget;
}

class HotpointWidget : public QWidget
{
    Q_OBJECT

public:
    explicit HotpointWidget(QWidget *parent = 0);
    ~HotpointWidget();
signals:
    void changed();
public slots:
    void setAttributes(const QSet<quint32> &, DocumentPtr);

    void setBias(double);
    void setTension(double);
    void setContinuity(double);
private:
    Ui::HotpointWidget *ui;

    DrawablePtr model_;
    DocumentPtr doc_;
};

#endif // HOTPOINTWIDGET_H
