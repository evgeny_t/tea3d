#ifndef DOCUMENT_H
#define DOCUMENT_H

#include <QObject>
#include <QVector>

#include "spline3djig.h"

class Document : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool dirty READ dirty WRITE setDirty NOTIFY dirtyChanged)
    Q_PROPERTY(QString filename READ filename WRITE setFilename NOTIFY filenameChanged)
public:
    explicit Document(QObject *parent = 0);
    Document(QString filename);

    Spline3DJig *jig() { return jig_.data(); }
    const QVector<DrawablePtr> &items() { return items_; }
    void addItem(DrawablePtr item) { items_.push_back(item); }
    void removeItem(quint32);

    bool save();
    bool save(QString filename);
    static QSharedPointer<Document> load(QString filename);

    DrawablePtr item(quint32);

    void addDebugItems();

    bool dirty() const;
    QString filename() const;

signals:
    void dirtyChanged(bool dirty);

    void filenameChanged(QString filename);

public slots:
    void startJig();
    void startJig(quint32);
    void stopJig();

    void setDirty(bool dirty);
    void setFilename(QString filename);

private:
    QVector<DrawablePtr> items_;
    Spline3DJigPtr jig_;

    bool m_dirty;
    QString m_filename;
};

DEFINE_OBJECT_PTR(Document);

#endif // DOCUMENT_H
