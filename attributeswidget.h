#ifndef ATTRIBUTESWIDGET_H
#define ATTRIBUTESWIDGET_H

#include <QSet>
#include <QWidget>

#include "document.h"

class AttributesWidget : public QWidget
{
    Q_OBJECT
public:
    explicit AttributesWidget(QWidget *parent = 0);

signals:

public slots:
    void setHotpoint();
    void setAttributes(const QSet<quint32> &, DocumentPtr);
};

#endif // ATTRIBUTESWIDGET_H
