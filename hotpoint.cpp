#include "hotpoint.h"

#include <QtDebug>

#include "irenderarea.h"

Hotpoint::Hotpoint(QWeakPointer<Spline3D> spline, int index) :
    Frustum(0.1f, 0.1f, 0.2f),
    spline_(spline),
    idx_(index)
{
    setColor(QColor(Qt::red));

    if (spline)
    {
        auto knot = spline.data()->knots().at(index);
        QMatrix4x4 newplace = placement();
        newplace.translate(knot);
        setPlacement(newplace);
    }
}

float Hotpoint::bias()
{
    if (spline_)
    {
        return spline_.data()->bias(idx_);
    }

    qWarning() << "Hotpoint::bias: " << "spline_ is null";
    return 0.0f;
}

float Hotpoint::tension()
{
    if (spline_)
    {
        return spline_.data()->tension(idx_);
    }

    qWarning() << "Hotpoint::tension: " << "spline_ is null";
    return 0.0f;
}

float Hotpoint::continuity()
{
    if (spline_)
    {
        return spline_.data()->continuity(idx_);
    }

    qWarning() << "Hotpoint::continuity: " << "spline_ is null";
    return 0.0f;
}

void Hotpoint::setBias(float v)
{
    if (spline_)
    {
        spline_.data()->setBias(idx_, v);
    }
    else
    {
        qWarning() << "Hotpoint::setBias: " << "spline_ is null";
    }
}

void Hotpoint::setTension(float v)
{
    if (spline_)
    {
        spline_.data()->setTension(idx_, v);
    }
    else
    {
        qWarning() << "Hotpoint::setTension: " << "spline_ is null";
    }
}

void Hotpoint::setContinuity(float v)
{
    if (spline_)
    {
        spline_.data()->setContinuity(idx_, v);
    }
    else
    {
        qWarning() << "Hotpoint::setContinuity: " << "spline_ is null";
    }
}

void Hotpoint::internalDraw(IRenderArea *f)
{
    // move center of the frustum to (0,0,0)
    QMatrix4x4 local;
    local.translate(0.0f, 0.0f, -0.1f);
    f->displace(placement() * local);
    Frustum::internalDraw(f);
}
