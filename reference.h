#ifndef REFERENCE_H
#define REFERENCE_H

#include <QVector>

#include "frustum.h"

class Reference : public Drawable
{
    Q_OBJECT
public:
    explicit Reference(QObject *parent = 0);

    Drawable *axisX() { return axes_[1].data(); }
    Drawable *axisY() { return axes_[0].data(); }
    Drawable *axisZ() { return axes_[2].data(); }
signals:

public slots:
protected:
    virtual void internalDrawChildren(IRenderArea *) override;
    virtual void setupVertexAttributePointers(IRenderArea *) override;
private:
    // (Y X Z) in this order
    DrawablePtr axes_[3];
};

DEFINE_OBJECT_PTR(Reference);

#endif // REFERENCE_H
