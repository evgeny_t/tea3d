#include "frustum.h"

#include <QOpenGLFunctions>

Frustum::Frustum(float b1, float b2, float h, QObject *parent) :
    Drawable(parent),
    b1_(b1),
    b2_(b2),
    h_(h)
{
    build();
}

//void Frustum::internalDraw(IRenderArea *f)
//{
//    f->glDrawElements(GL_TRIANGLES, indices_.size(), GL_UNSIGNED_INT, 0);
//}

//void Frustum::setupVertexAttributePointers(IRenderArea *f)
//{
//    f->glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(QVector3D), 0);
//    f->glEnableVertexAttribArray(0);
//}

void Frustum::build()
{
    vertices_ = QVector<QVector3D> {
        QVector3D(b1_, b1_, 0),
        QVector3D(b1_, -b1_, 0),
        QVector3D(-b1_, -b1_, 0),
        QVector3D(-b1_, b1_, 0),

        QVector3D(b2_, b2_, h_),
        QVector3D(b2_, -b2_, h_),
        QVector3D(-b2_, -b2_, h_),
        QVector3D(-b2_, b2_, h_)
    };

    indices_ = QVector<GLuint> {
        0, 4, 5,
        0, 1, 5,
        1, 5, 6,
        1, 2, 6,
        2, 6, 7,
        2, 3, 7,
        3, 4, 7,
        0, 3, 4,
        4, 5, 6,
        4, 6, 7,
        0, 1, 3,
        1, 2, 3
    };
}
