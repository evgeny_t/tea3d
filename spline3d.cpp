#include "spline3d.h"

#include <QtDebug>
#include <QOpenGLFunctions>
#include <QOpenGLShader>

#include "irenderarea.h"

QVector3D pointOnCurve(const QVector3D &p0, const QVector3D &p1,
                     const QVector3D &m0, const QVector3D &m1, qreal t)
{
    qreal powers[4] = { 1.0 };
    for (int i = 1; i < 4; ++i)
    {
        powers[i] = powers[i - 1] * t;
    }

    qreal h00 = 2.0 * powers[3] - 3.0 * powers[2] + 1.0,
          h10 = powers[3] - 2.0 * powers[2] + powers[1],
          h01 = -2.0 * powers[3] + 3.0 * powers[2],
          h11 = powers[3] - powers[2];
    return h00 * p0 + h10 * m0 + h01 * p1 + h11 * m1;
}


Spline3D::Spline3D(QObject *parent) :
    Drawable(parent)
{
    static QRgb colors[3] = {
        0x339900,
        0xFF9900,
        0x3399FF
    };

    setColor(QColor(colors[qrand() % 3]));
}

Spline3D &Spline3D::add(const QVector3D &v)
{
    knots_.push_back(v);
    tension_.push_back(0.0);
    bias_.push_back(0.0);
    continuity_.push_back(0.0);
    calculateTangents();
    calculateVertices();

    return *this;
}

void Spline3D::removeKnot(int i)
{
    knots_.remove(i);
    tension_.remove(i);
    bias_.remove(i);
    continuity_.remove(i);
    calculateTangents();
    calculateVertices();
}

void Spline3D::popBack()
{
    knots_.pop_back();
    tension_.pop_back();
    bias_.pop_back();
    continuity_.pop_back();
    calculateTangents();
    calculateVertices();
}

const QVector<QVector3D> &Spline3D::knots()
{
    return knots_;
}

void Spline3D::shift(int i, const QVector3D &delta)
{
    if (0 <= i && i < knots_.size())
    {
        knots_[i] += delta;
        calculateTangents();
        calculateVertices();
    }
}

void Spline3D::moveKnot(int i, const QVector3D &p)
{
    if (0 <= i && i < knots_.size())
    {
        knots_[i] = p;
        calculateTangents();
        calculateVertices();
    }
}

float Spline3D::bias(int i)
{
    if (0 <= i && i < bias_.size())
    {
        return bias_[i];
    }

    qWarning() << "Spline3D::bias: " << "argument out of range: " << i;
    return 0.0f;
}

float Spline3D::tension(int i)
{
    if (0 <= i && i < tension_.size())
    {
        return tension_[i];
    }

    qWarning() << "Spline3D::tension: " << "argument out of range: " << i;
    return 0.0f;
}

float Spline3D::continuity(int i)
{
    if (0 <= i && i < continuity_.size())
    {
        return continuity_[i];
    }

    qWarning() << "Spline3D::continuity: " << "argument out of range: " << i;
    return 0.0f;
}

void Spline3D::setBias(int i, float v)
{
    if (0 <= i && i < bias_.size())
    {
        bias_[i] = v;
        calculateTangents();
        calculateVertices();
    }
    else
    {
        qWarning() << "Spline3D::setBias: " << "argument out of range: " << i;
    }
}

void Spline3D::setTension(int i, float v)
{
    if (0 <= i && i < tension_.size())
    {
        tension_[i] = v;
        calculateTangents();
        calculateVertices();
    }
    else
    {
        qWarning() << "Spline3D::setTension: " << "argument out of range: " << i;
    }
}

void Spline3D::setContinuity(int i, float v)
{
    if (0 <= i && i < continuity_.size())
    {
        continuity_[i] = v;
        calculateTangents();
        calculateVertices();
    }
    else
    {
        qWarning() << "Spline3D::setContinuity: " << "argument out of range: " << i;
    }
}

QSharedPointer<Spline3D> Spline3D::clone()
{
    auto copy = QSharedPointer<Spline3D>::create();
    copy->in_ = in_;
    copy->out_ = out_;
    copy->knots_ = knots_;
    copy->tension_ = tension_;
    copy->bias_ = bias_;
    copy->continuity_ = continuity_;
    copy->setPlacement(placement());
    return copy;
}

void Spline3D::internalDraw(IRenderArea *f)
{
    f->glDrawElements(GL_LINE_STRIP, indices_.size(), GL_UNSIGNED_INT, 0);
}

void Spline3D::setupVertexAttributePointers(IRenderArea *f)
{
    f->glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(QVector3D), 0);
    f->glEnableVertexAttribArray(0);
}

void Spline3D::calculateTangents()
{
    if (knots_.size() < 2)
    {
        return;
    }

    QVector<QVector3D> knots = knots_;
    knots.insert(0, knots.first());
    knots.push_back(knots.back());
    knots.push_back(knots.back());

    in_.resize(knots.size() - 3);
    out_.resize(in_.size());

    for (int i = 0; i < in_.size(); ++i)
    {
        qreal oneMinusTension = 1 - tension_[i];
        QVector3D *current = knots.begin() + i + 1;
        qreal oneMinusB = 1 - bias_[i],
              onePlusB = 1 + bias_[i],
              onePlusC = 1 + continuity_[i],
              oneMinusC = 1 - continuity_[i];
        out_[i] = 0.5 * oneMinusTension *
                  (onePlusB * onePlusC * (*current - *(current - 1)) +
                  oneMinusB * oneMinusC * (*(current + 1) - *current));
        in_[i] = 0.5 * oneMinusTension *
                 (onePlusB * oneMinusC * (*(current + 1) - *current) +
                 oneMinusB * onePlusC * (*(current + 2) - *(current + 1)));
    }
}

void Spline3D::calculateVertices()
{
    if (knots_.size() < 2)
    {
        return;
    }

    vertices_.clear();
    vertices_.push_back(knots_.first());
    indices_.clear();
    indices_.push_back(0);

    for (int i = 0; i < in_.size() - 1; ++i)
    {
        for (qreal T = SplineApproxStep; T < 1.0; T += SplineApproxStep)
        {
            auto next = pointOnCurve(knots_[i], knots_[i + 1], out_[i], in_[i], T);
            vertices_.push_back(next);
            indices_.push_back(vertices_.size() - 1);
        }
    }

    vertices_.push_back(knots_.back());
    indices_.push_back(vertices_.size() - 1);
}

