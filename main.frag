#version 330 core

uniform vec3 object_color;
uniform int object_id;
uniform sampler2D qt_Texture0;

uniform int widen;

uniform vec2 resolution;

out vec4 color;

void main()
{
    if (object_id == 0) {
        color = vec4(object_color, 1.0f);
    } else {
        if (widen == 0) {
            int x = object_id & 0xff;
            int y = (object_id >> 8) & 0xff;
            int z = (object_id >> 16) & 0xff;
            color = vec4(z / 255.0, y / 255.0, x / 255.0, 1.0f);
        } else {
            // I prolly do smt wrong
            vec4 maxc = vec4(0, 0, 0, 0);
            float maxl = 0.0f;
            float bgr = length(vec4(1.0f, 1.0f, 1.0f, 1.0f));
            for (int i = -3; i <= 3; ++i) {
                for (int j = -3; j <= 3; ++j) {
                    vec2 p = vec2(gl_FragCoord.xy) + vec2(i, j);
                    if (p.x >= 0 && p.y >= 0) {
                        p = p.xy / resolution.xy;
                        vec4 c = texture2D(qt_Texture0, p);
                        if (length(c) >= maxl && length(c) < bgr) {
                            maxl = length(c);
                            maxc = c;
                        }
                    }
                }
            }

            color = maxc;
        }
    }
}


//varying vec4 qt_TexCoord0;

//void main(void)
//{
//    gl_FragColor = texture2D(qt_Texture0, qt_TexCoord0.st);
//}
