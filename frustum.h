#ifndef FRUSTUM_H
#define FRUSTUM_H

#include "drawable.h"

class Frustum : public Drawable
{
    Q_OBJECT
public:
    explicit Frustum(float b1 = 0.02, float b2 = 0.01, float h = 1.0, QObject *parent = 0);

signals:
public slots:
//protected:
//    virtual void internalDraw(IRenderArea *) override;
//    virtual void setupVertexAttributePointers(IRenderArea *) override;
private:
    void build();

    float b1_;
    float b2_;
    float h_;
};

DEFINE_OBJECT_PTR(Frustum);

#endif // FRUSTUM_H
