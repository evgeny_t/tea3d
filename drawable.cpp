#include "drawable.h"

#include <QOpenGLFunctions>
#include <QColor>
#include <QVector3D>

#include "irenderarea.h"

Drawable::Drawable(QObject *parent) :
    QObject(parent),
    vbo_(QOpenGLBuffer::VertexBuffer),
    ebo_(QOpenGLBuffer::IndexBuffer),
    id_(((quint32)qrand() & 0xfffffe) + 1)
{
    vbo_.create();
    vao_.create();
    ebo_.create();
}

Drawable::~Drawable()
{
    vao_.destroy();
    vbo_.destroy();
    ebo_.destroy();
}

bool Drawable::hasChild(quint32)
{
    return false;
}

void Drawable::removeChild(quint32)
{
}

DrawablePtr Drawable::child(quint32)
{
    return DrawablePtr();
}


void Drawable::draw(IRenderArea *f)
{
    if (vertices_.size() && indices_.size())
    {
        vao_.bind();

        vbo_.bind();
        vbo_.allocate(vertices_.begin(), sizeof(QVector3D) * vertices_.size());

        ebo_.bind();
        ebo_.allocate(indices_.begin(), sizeof(GLuint) * indices_.size());

        setupVertexAttributePointers(f);

        vbo_.release();
        vao_.release();

        vao_.bind();
        internalDraw(f);
        vao_.release();
    }

    internalDrawChildren(f);
}

void Drawable::setColor(const QColor &color)
{
    color_ = QVector3D(color.redF(), color.greenF(), color.blueF());
}

//void Drawable::rebuild(QVector<QVector3D> &, QVector<GLuint> &)
//{
//}

void Drawable::internalDraw(IRenderArea *f)
{
    if (indices_.size())
    {
        f->glDrawElements(GL_TRIANGLES, indices_.size(), GL_UNSIGNED_INT, 0);
    }
}

void Drawable::setupVertexAttributePointers(IRenderArea *f)
{
    if (indices_.size())
    {
        f->glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(QVector3D), 0);
        f->glEnableVertexAttribArray(0);
    }
}

void Drawable::internalDrawChildren(IRenderArea *)
{
}
