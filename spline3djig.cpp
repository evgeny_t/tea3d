#include "spline3djig.h"
#include "irenderarea.h"

#include <QMouseEvent>

Spline3DJig::Spline3DJig() :
    state_(JigState_New),
    spline_(Spline3DPtr::create())
{
}

Spline3DJig::Spline3DJig(Spline3DPtr ptr) :
    state_(JigState_Edit),
    spline_(ptr)
{
    const auto & knots = ptr->knots();
    for (int i = 0; i < knots.size(); ++i)
    {
        hotpoints_.push_back(makeHotpoint(i));
    }
}

bool Spline3DJig::hasChild(quint32 id)
{
    for (int i = 0; i < hotpoints_.size(); ++i)
    {
        if (hotpoints_[i] &&
            (hotpoints_[i]->id() == id || hotpoints_[i]->hasChild(id)))
        {
            return true;
        }
    }

    return false;
}

DrawablePtr Spline3DJig::child(quint32 childId)
{
    for (int i = 0; i < hotpoints_.size(); ++i)
    {
        if (hotpoints_[i])
        {
            if (hotpoints_[i]->id() == childId)
            {
                return hotpoints_[i];
            }
            else if (hotpoints_[i]->hasChild(childId))
            {
                return hotpoints_[i]->child(childId);
            }
        }
    }

    return DrawablePtr();
}

void Spline3DJig::removeChild(quint32 childId)
{
    for (int i = 0; i < hotpoints_.size(); ++i)
    {
        if (hotpoints_[i] && hotpoints_[i]->id() == childId)
        {
            spline_->removeKnot(i);
            hotpoints_.removeAt(i);
            if (references_.contains(childId))
            {
                references_.remove(childId);
            }
        }
    }
}

void Spline3DJig::keyPressed(Qt::Key key)
{
    switch (key) {
    case Qt::Key_Insert:
    {
        state_ = JigState_New;
        auto &knots = spline_->knots();
        if (knots.size())
        {
            addHotpoint(knots.back());
        }
        break;
    }
    default:
        break;
    }
}

void Spline3DJig::mouseDown(QMouseEvent *e, IRenderArea *f)
{
    if (state_ == JigState_Edit)
    {
        if (references_.size())
        {
            const auto &ss = f->selectionset();
            ReferencePtr ref = references_.first();

            if (ss.contains(ref->axisY()->id()))
            {
                // Y
                auto local = ref->placement() * ref->axisY()->placement();
                down_ = f->cursorToWorld(local, e->pos());
                phantom_ = spline_->clone();
            }
            else if (ss.contains(ref->axisX()->id()))
            {
                // X
                auto local = ref->placement() * ref->axisX()->placement();
                down_ = f->cursorToWorld(local, e->pos());
                phantom_ = spline_->clone();
            }
            else if (ss.contains(ref->axisZ()->id()))
            {
                // Z
                auto local = ref->placement() * ref->axisZ()->placement();
                down_ = f->cursorToWorld(local, e->pos());
                phantom_ = spline_->clone();
            }
        }
    }
}

void Spline3DJig::mouseUp(const QVector3D &modelPlace, IRenderArea *f)
{
    Q_ASSERT(f);

    if (state_ == JigState_New)
    {
        addHotpoint(modelPlace);
    }
    else if (state_ == JigState_Edit)
    {
        if (phantom_)
        {
            shiftHotpoint(references_.begin().key(), d, false);
            phantom_.reset();
        }

        if (f->selectionset().size())
        {
            references_.clear();
            auto id = *f->selectionset().begin();
            for (int i = 0; i < hotpoints_.size(); ++i)
            {
                if (hotpoints_[i]->id() == id || hotpoints_[i]->hasChild(id))
                {
                    auto ptr = ReferencePtr::create();
                    ptr->setPlacement(hotpoints_[i]->placement());
                    references_.insert(id, ptr);
                }
            }
        }
    }
}

void Spline3DJig::mouseMove(QMouseEvent *e, IRenderArea *f)
{
    auto modelPlace = f->cursorToWorld(1, e->pos());
    if (state_ == JigState_New)
    {
        if (!spline_->knots().size())
        {
            spline_->add(modelPlace);
            hotpoints_.push_back(makeHotpoint(0));
        }

        spline_->moveKnot(spline_->knots().size() - 1, modelPlace);

        QMatrix4x4 newplace;
        newplace.translate(modelPlace);
        hotpoints_.back()->setPlacement(newplace);
    }
    else if (state_ == JigState_Edit)
    {
        if (references_.size() && (e->buttons() & Qt::LeftButton))
        {
            QVector3D move;
            const auto &ss = f->selectionset();
            ReferencePtr ref = *references_.begin();
            quint32 id = references_.begin().key();

            if (ss.contains(ref->axisY()->id()))
            {
                auto local = ref->placement() * ref->axisY()->placement();
                // Y
                move = f->cursorToWorld(local, e->pos());
                d = QVector3D(0, move[2] - down_[2], 0);

                shiftHotpoint(id, d, true);
            }
            else if (ss.contains(ref->axisX()->id()))
            {
                auto local = ref->placement() * ref->axisX()->placement();
                // X
                move = f->cursorToWorld(local, e->pos());
                d = QVector3D(move[2] - down_[2], 0, 0);

                shiftHotpoint(id, d, true);
            }
            else if (ss.contains(ref->axisZ()->id()))
            {
                auto local = ref->placement() * ref->axisZ()->placement();
                // Z
                move = f->cursorToWorld(local, e->pos());
                d = QVector3D(0, 0, move[2] - down_[2]);

                shiftHotpoint(id, d, true);
            }
        }
    }
}

void Spline3DJig::cancel()
{
}

void Spline3DJig::done()
{
    if (spline_ && state_ == JigState_New)
    {
        spline_->popBack();
    }

    state_ = JigState_Done;
}

void Spline3DJig::internalDrawChildren(IRenderArea *f)
{
    QMatrix4x4 place;
    spline_->draw(f);

    if (phantom_)
    {
        f->drawObject(phantom_.data());
    }

    for (int i = 0; i < hotpoints_.size(); ++i)
    {
        place = placement() * hotpoints_[i]->placement();
        f->displace(place);
        f->drawObject(hotpoints_[i].data());
    }

    for (decltype (references_)::Iterator i = references_.begin(); i != references_.end(); ++i)
    {
        f->drawObject(i.value().data());
    }
}

void Spline3DJig::addHotpoint(const QVector3D &where)
{
    spline_->add(where);
    hotpoints_.push_back(makeHotpoint(spline_->knots().size() - 1));
}

HotpointPtr Spline3DJig::makeHotpoint(int index)
{
    return HotpointPtr::create(spline_.toWeakRef(), index);
}

void Spline3DJig::shiftHotpoint(quint32 id, const QVector3D &delta, bool phantom)
{
    QMatrix4x4 m;
    for (int i = 0; i < hotpoints_.size(); ++i)
    {
        if (hotpoints_[i]->id() == id || hotpoints_[i]->hasChild(id))
        {
            m = hotpoints_[i]->placement();
            m.translate(delta);
            hotpoints_[i]->setPlacement(m);
            if (phantom)
            {
                phantom_->shift(i, delta);
            }
            else
            {
                spline_->moveKnot(i, phantom_->knots().at(i));
            }

            if (references_.contains(id))
            {
                ReferencePtr ref = references_[id];
                m = ref->placement();
                m.translate(delta);
                ref->setPlacement(m);
            }
        }
    }
}

