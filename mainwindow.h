#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include "application.h"

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(Application &app, QWidget *parent = 0);
    ~MainWindow();

    void updateWindowTitle(const QString &filename, bool dirty);

    // QWidget interface
protected:
    virtual void closeEvent(QCloseEvent *) override;

    Application &m_app;
};

#endif // MAINWINDOW_H
