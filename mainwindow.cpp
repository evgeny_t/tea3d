#include "mainwindow.h"

#include <QDesktopWidget>
#include <QLayout>
#include <QToolBar>
#include <QAction>
#include <QDockWidget>
#include <QtEvents>
#include <QApplication>

#include "document.h"
#include "renderarea.h"
#include "debugarea.h"
#include "attributeswidget.h"
#include "hotpointwidget.h"

MainWindow::MainWindow(Application &app, QWidget *parent)
    : QMainWindow(parent),
      m_app(app)
{
    setMinimumSize(960, 720);
    auto place = (QDesktopWidget().availableGeometry(this).size() - size()) / 2;
    move(place.width(), place.height());

    RenderArea *area = new RenderArea(this);
    auto *grid = new QGridLayout();
    grid->addWidget(area, 0, 0);

    if (app.debug())
    {
        DebugArea *fboArea = new DebugArea(this);
        connect(area, &RenderArea::rendered, fboArea, &DebugArea::updateArea);
        grid->addWidget(fboArea, 0, 1);
    }

    QWidget *central = new QWidget;
    central->setLayout(grid);
    setCentralWidget(central);

    QToolBar *main = this->addToolBar("Main");

    QAction *a0 = new QAction(QIcon(":/appbar.page.new.svg"), tr("&New"), this);
    main->addAction(a0);

    QAction *a2 = new QAction(QIcon(":/appbar.cabinet.files.svg"), tr("&Open"), this);
    main->addAction(a2);

    QAction *a3 = new QAction(QIcon(":/appbar.save.svg"), tr("&Save"), this);
    main->addAction(a3);

    connect(a0, &QAction::triggered, &app, &Application::newDocument);
    connect(a2, &QAction::triggered, &app, &Application::open);
    connect(a3, &QAction::triggered, &app, &Application::saveCurrent);

    QAction *a1 = new QAction(QIcon(":/appbar.vector.line.spline.svg"), tr("&Spline"), this);
    main->addAction(a1);
    connect(a1, &QAction::triggered, area, &RenderArea::startJig);

    auto bindDocumentSignals = [this, area, &app](QWeakPointer<Document> doc) {
        if (doc)
        {
            qDebug() << "connect signals of" << doc.data()->filename();
            connect(doc.data(), &Document::dirtyChanged, [doc, this](bool) {
                updateWindowTitle(doc.data()->filename(), doc.data()->dirty());
            });
        }
    };

    connect(area, &RenderArea::ready, [this, area, bindDocumentSignals, &app]() {
        if (app.current())
        {
            auto currentDoc = DocumentPtr(app.current());
            bindDocumentSignals(currentDoc);

            updateWindowTitle(currentDoc->filename(), currentDoc->dirty());
            area->setDocument(currentDoc);
            if (app.debug())
                currentDoc->addDebugItems();
        }
    });

    connect(&app, &Application::opened,
            [this, area, bindDocumentSignals](QWeakPointer<Document> doc) {
        bindDocumentSignals(doc);
        updateWindowTitle(doc.data()->filename(), doc.data()->dirty());
        area->setDocument(doc);
    });

    QDockWidget *dock0 = new QDockWidget("Attributes", this);
    addDockWidget(Qt::LeftDockWidgetArea, dock0);
    HotpointWidget *hotpointsWidget = new HotpointWidget(dock0);
    dock0->setWidget(hotpointsWidget);
    dock0->setMinimumWidth(hotpointsWidget->width());
    dock0->hide();

    connect(area, &RenderArea::selected, hotpointsWidget, &HotpointWidget::setAttributes);
    connect(hotpointsWidget, SIGNAL(changed()), area, SLOT(update()));
}

MainWindow::~MainWindow()
{

}

void MainWindow::updateWindowTitle(const QString &filename, bool dirty)
{
    setWindowTitle(QString("tea3d - %1%2")
                   .arg(filename)
                   .arg(dirty ? "*" : ""));
}

void MainWindow::closeEvent(QCloseEvent *e)
{
    if (!m_app.tryCloseCurrent())
    {
        e->ignore();
    }
}
