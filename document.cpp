#include "document.h"

#include <QFile>
#include <QVector3D>

#include "reference.h"
#include "spline3d.h"

Document::Document(QObject *parent) : QObject(parent)
{
    static int newDocumentIndex = 1;
    static QString format("splines-%1.tea3d");

    while (QFile::exists(m_filename = format.arg(newDocumentIndex++)));
    m_dirty = true;
}

Document::Document(QString filename) :
    m_filename(filename)
{
}

void Document::removeItem(quint32 id)
{
    if (jig_->id() == id)
    {
        removeItem(jig_->spline()->id());
        jig_.reset();
    }
    else if (jig_->hasChild(id))
    {
        jig_->removeChild(id);
    }
    else
    {
        for (int i = 0; i < items_.size(); ++i)
        {
            if (items_[i]->id() == id)
            {
                if (jig_->spline()->id() == id)
                {
                    jig_.reset();
                }

                items_.removeAt(i);
            }
        }
    }
}

bool Document::save()
{
    return save(filename());
}

bool Document::save(QString filename)
{
    QFile output(filename);
    if (!output.open(QFile::WriteOnly))
    {
        return false;
    }

    QTextStream stream(&output);
    QVector<Spline3DPtr> splines;
    for (int i = 0; i < items_.size(); ++i)
    {
        auto spline = items_[i].dynamicCast<Spline3D>();
        if (spline)
        {
            splines.push_back(spline);
        }
    }

    stream << splines.size() << endl;
    for (int i = 0; i < splines.size(); ++i)
    {
        auto &knots = splines[i]->knots();
        stream << knots.size() << endl;
        for (int j = 0; j < knots.size(); ++j)
        {
            for (int dim = 0; dim < 3; ++dim)
            {
                stream << knots[j][dim] << " ";
            }
        }

        stream << endl;
        for (int j = 0; j < knots.size(); ++j)
            stream << splines[i]->bias(j) << " ";
        stream << endl;
        for (int j = 0; j < knots.size(); ++j)
            stream << splines[i]->continuity(j) << " ";
        stream << endl;
        for (int j = 0; j < knots.size(); ++j)
            stream << splines[i]->tension(j) << " ";
        stream << endl;
    }

    setDirty(false);
    return true;
}

DocumentPtr Document::load(QString filename)
{
    QFile input(filename);
    if (!input.open(QFile::ReadOnly))
    {
        return DocumentPtr();
    }

    QTextStream stream(&input);
    DocumentPtr document = DocumentPtr::create(filename);
    float x, y, z;
    int splinesCount, knotsCount;
    stream >> splinesCount;
    for (int i = 0; i < splinesCount; ++i)
    {
        QVector<QVector3D> knots;
        QVector<qreal> tension;
        QVector<qreal> bias;
        QVector<qreal> continuity;
        Spline3DPtr spline = Spline3DPtr::create();
        stream >> knotsCount;
        for (int j = 0; j < knotsCount; ++j)
        {
            stream >> x >> y >> z;
            knots.push_back(QVector3D(x, y, z));
        }

        for (int j = 0; j < knotsCount; ++j)
        {
            stream >> x;
            bias.push_back(x);
        }

        for (int j = 0; j < knotsCount; ++j)
        {
            stream >> x;
            continuity.push_back(x);
        }

        for (int j = 0; j < knotsCount; ++j)
        {
            stream >> x;
            tension.push_back(x);
        }

        spline->knots_ = knots;
        spline->bias_ = bias;
        spline->continuity_ = continuity;
        spline->tension_ = tension;
        spline->calculateTangents();
        spline->calculateVertices();
        document->addItem(spline);
        qDebug() << "Document::load: " << spline->id();
    }

    document->setDirty(false);
    return document;
}

DrawablePtr Document::item(quint32 id)
{
    for (int i = 0; i < items_.size(); ++i)
    {
        if (items_[i]->id() == id)
        {
            return items_[i];
        }

        if (items_[i]->hasChild(id))
        {
            return items_[i]->child(id);
        }
    }

    if (jig_ && jig_->id() == id)
    {
        return jig_;
    }
    else if (jig_ && jig_->hasChild(id))
    {
        return jig_->child(id);
    }

    return DrawablePtr();
}

void Document::addDebugItems()
{
    Spline3DPtr spline = Spline3DPtr(new Spline3D);
    spline->setColor(QColor(Qt::red));
    spline->add(QVector3D(0, 0, 0));
    spline->add(QVector3D(100, 0, 0));
    spline->add(QVector3D(100, 100, 0));
    spline->add(QVector3D(100, 100, 100));
    spline->add(QVector3D(0, 100, 100));
    spline->add(QVector3D(0, 100, 0));
    items_.push_back(spline);

    spline = Spline3DPtr(new Spline3D);
    spline->setColor(QColor(Qt::green));
    spline->add(QVector3D(0, 0, 0));
    spline->add(QVector3D(0.1, 0, 0));
    spline->add(QVector3D(0.1, 0.1, 0));
    spline->add(QVector3D(0.1, 0.1, 0.1));
    spline->add(QVector3D(0, 0.1, 0.1));
    spline->add(QVector3D(0, 0.1, 0));
    items_.push_back(spline);

    spline = Spline3DPtr(new Spline3D);
    spline->setColor(QColor(Qt::cyan));
    float x = -2;
    for (int i = 0; i < 40; ++i)
    {
        if (i & 1)
        {
            spline->add(QVector3D(x, -2, 0));
        }
        else
        {
            spline->add(QVector3D(x, -2, 0));
            spline->add(QVector3D(x, 2, 0));
            spline->add(QVector3D(x + 0.2, 2, 0));
        }

        x += 0.2;
    }

    items_.push_back(spline);

    spline = Spline3DPtr(new Spline3D);
    spline->setColor(QColor(Qt::magenta));
    x = -2;
    for (int i = 0; i < 40; ++i)
    {
        if (i & 1)
        {
            spline->add(QVector3D(x, -2, 3));
        }
        else
        {
            spline->add(QVector3D(x, -2, 3));
            spline->add(QVector3D(x, 2, 3));
            spline->add(QVector3D(x + 0.2, 2, 3));
        }

        x += 0.2;
    }
    items_.push_back(spline);

    items_.push_back(ReferencePtr(new Reference));
    items_.last()->setPlacement(
                QMatrix4x4(
                    1,   0,  0,  10,
                    0,   1,  0,  0,
                    0,   0,  1,  0,
                    0,   0,  0,  1
                    ));
}

bool Document::dirty() const
{
    return m_dirty;
}

QString Document::filename() const
{
    return m_filename;
}

void Document::startJig(quint32 id)
{
    for (int i = 0; i < items_.size(); ++i)
    {
        if (items_[i] && items_[i]->id() == id)
        {
            auto spline = items_[i].dynamicCast<Spline3D>();
            if (spline)
            {
                jig_ = Spline3DJigPtr::create(spline);
                return;
            }
        }
    }
}

void Document::startJig()
{
    jig_ = Spline3DJigPtr::create();
}

void Document::stopJig()
{
    if (jig_)
    {
        jig_->done();
        items_.push_back(jig_->spline());
        jig_.reset();
    }
}

void Document::setDirty(bool dirty)
{
    if (m_dirty == dirty)
        return;

    m_dirty = dirty;
    emit dirtyChanged(dirty);
}

void Document::setFilename(QString filename)
{
    if (m_filename == filename)
        return;

    m_filename = filename;
    emit filenameChanged(filename);
}
