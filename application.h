#ifndef APPLICATION_H
#define APPLICATION_H

#include <QApplication>
#include <QVector>
#include <QObject>
#include <QSharedPointer>

#include "document.h"

const QString Filter = "*.tea3d";

class Application : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool debug READ debug WRITE setDebug NOTIFY debugChanged)
public:
    Application(QApplication *app);

    QWeakPointer<Document> current() { return current_; }
    bool tryCloseCurrent();

    bool debug() const;

signals:
    void opened(QWeakPointer<Document>);
    void debugChanged(bool debug);

public slots:
    void newDocument();
    bool saveCurrent();
    void open();
    void setDebug(bool debug);

private:
    DocumentPtr current_;
    bool m_debug;
};

#endif // APPLICATION_H
