#ifndef SPLINE3DJIG_H
#define SPLINE3DJIG_H

#include "spline3d.h"
#include "frustum.h"
#include "reference.h"
#include "hotpoint.h"

class QMouseEvent;

class Spline3DJig : public Drawable
{
public:
    enum JigState
    {
        JigState_New,
        JigState_Edit,
        JigState_Done,
        JigState_Cancel
    };

    Spline3DJig();
    Spline3DJig(Spline3DPtr);

    virtual bool hasChild(quint32) override;
    virtual DrawablePtr child(quint32) override;
    virtual void removeChild(quint32) override;

    void keyPressed(Qt::Key);
    void mouseDown(QMouseEvent *e, IRenderArea *f);
    void mouseUp(const QVector3D &modelPlace, IRenderArea *f);
    void mouseMove(QMouseEvent *e, IRenderArea *f);
    void cancel();
    void done();

    Spline3DPtr spline() { return spline_; }
protected:
    virtual void internalDrawChildren(IRenderArea *) override;

private:
    void addHotpoint(const QVector3D &);
    HotpointPtr makeHotpoint(int);
    void shiftHotpoint(quint32 id, const QVector3D &delta, bool phantom);
    JigState state_;
    Spline3DPtr spline_;
    Spline3DPtr phantom_;

    QVector3D d; // shift of hotpoint

    QVector<HotpointPtr> /*QVector<FrustumPtr>*/ hotpoints_;
    QMap<quint32, ReferencePtr> references_;
//    FrustumPtr last_;

    QVector3D down_;
};

DEFINE_OBJECT_PTR(Spline3DJig);

#endif // SPLINE3DJIG_H
