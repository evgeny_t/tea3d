#ifndef SPLINE3D_H
#define SPLINE3D_H

#include <QVector>
#include <QVector3D>
#include <QSharedPointer>

#include "drawable.h"

class Document;
class QOpenGLFunctions;

const float SplineApproxStep = 1e-3;

class Spline3D : public Drawable
{
    Q_OBJECT
public:
    friend class Document;

    explicit Spline3D(QObject *parent = 0);

    Spline3D &add(const QVector3D &);
    void removeKnot(int);
    void popBack();
    const QVector<QVector3D> &knots();
    void shift(int, const QVector3D &);
    void moveKnot(int, const QVector3D &);

    float bias(int);
    float tension(int);
    float continuity(int);

    void setBias(int, float);
    void setTension(int, float);
    void setContinuity(int, float);

    QSharedPointer<Spline3D> clone();
signals:

public slots:

protected:
    virtual void internalDraw(IRenderArea *) override;
    virtual void setupVertexAttributePointers(IRenderArea *) override;

private:
    void calculateTangents();
    void calculateVertices();

    QVector<QVector3D> in_;
    QVector<QVector3D> out_;
    QVector<QVector3D> knots_;
    QVector<qreal> tension_;
    QVector<qreal> bias_;
    QVector<qreal> continuity_;
};

DEFINE_OBJECT_PTR(Spline3D);

#endif // SPLINE3D_H
