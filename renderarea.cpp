#include "renderarea.h"

#include <QWheelEvent>
#include <QFile>
#include <QtMath>
#include <QtDebug>
#include <QOpenGLFramebufferObject>

#include "reference.h"

class Camera
{
public:
    Camera() :
        eye(10, 40, 10),
        center(0, 0, 0),
        up(0, 1, 0)
    {
    }

    void setViewport(int w, int h)
    {
        width_ = w;
        height_ = h;
    }

    QMatrix4x4 matrix()
    {
        auto temp = eye;
        if (dragging_)
            temp = rot_ * eye;

        QMatrix4x4 m;
        m.setToIdentity();
        m.lookAt(/*eye*/temp, center, up);
        return m;
    }

    void pan(int dx, int dy)
    {
        center -= QVector3D(float(dx), float(dy), 0.0) / 100.0;
        eye -= QVector3D(float(dx), float(dy), 0.0) / 100.0;
    }

    void zoom(int inOut)
    {
        QVector3D forward = eye - center;
        forward.normalize();
        eye -= forward * qMin(inOut * 1.0, (eye - center).length() - 0.2);
    }

    void start(const QPoint &p)
    {
        start_ = end_ = p;
    }

    void end(const QPoint &p)
    {
        dragging_ = true;
        end_ = p;

        QVector3D v0(start_.x() / width_ - 0.5, -start_.y() / height_ + 0.5, 0);
        QVector3D v1(end_.x() / width_ - 0.5, -end_.y() / height_ + 0.5, 0);
        v0 *= 2.0;
        v1 *= 2.0;

        if (v0.length() > 1.0)
            v0.normalize();
        if (v1.length() > 1.0)
            v1.normalize();

        v0[2] = sqrt(1.0 - v0.lengthSquared());
        v1[2] = sqrt(1.0 - v1.lengthSquared());

//        auto rv = QVector3D::crossProduct(v0, v1);
//        auto w = QVector3D::dotProduct(v0, v1);

        rot_.setToIdentity();
        rot_.rotate((v1 - v0).x() * 100, QVector3D(0, -1, 0));
        rot_.rotate((v1 - v0).y() * 100, QVector3D(1, 0, 0));
    }

    void update()
    {
        eye = rot_ * eye;
        dragging_ = false;
    }

private:
    QVector3D eye;
    QVector3D center;
    QVector3D up;

    QMatrix4x4 rot_;

    QPoint start_, end_;
    bool dragging_ = false;
    float width_ = 0, height_ = 0;
} camera;

RenderArea::RenderArea(QWidget *parent) : QOpenGLWidget(parent)
{
    setMouseTracking(true);
    setFocusPolicy(Qt::WheelFocus);
}

RenderArea::~RenderArea()
{
}

void RenderArea::displace(const QMatrix4x4 &m)
{
    glUniformMatrix4fv(sp_.uniformLocation("placement"), 1, GL_FALSE, m.data());
}

void RenderArea::setObjectColor(const QVector3D &c)
{
    glUniform3fv(sp_.uniformLocation("object_color"), 1, (GLfloat*)(&c));
}

void RenderArea::setDocument(DocumentPtr document)
{
    document_ = document;
    update();
}

void RenderArea::startJig()
{
    document_->startJig();
    update();
}

void RenderArea::initializeGL()
{
    makeCurrent();
    initializeOpenGLFunctions();

    connect(context(), &QOpenGLContext::aboutToBeDestroyed, this, &RenderArea::cleanUp);

    glClearColor(1.0f, 1.0f, 1.0f, 1.0f);

    objects_.push_back(ReferencePtr(new Reference));

    bool ok;

    QFile vert(":/main.vert");
    QFile frag(":/main.frag");
    vert.open(QFile::ReadOnly);
    frag.open(QFile::ReadOnly);

    ok = sp_.addShaderFromSourceCode(QOpenGLShader::Vertex, vert.readAll());
    qDebug() << "RenderArea::initializeGL: " << "QOpenGLShader::Vertex: " << ok;
    ok = sp_.addShaderFromSourceCode(QOpenGLShader::Fragment, frag.readAll());
    qDebug() << "RenderArea::initializeGL: " << "QOpenGLShader::Fragment: " << ok;
    ok = sp_.link();
    qDebug() << "RenderArea::initializeGL: " << "link: " << ok;

    resizeGL(width(), height());

    model_.setToIdentity();
    model_2.setToIdentity();

    glEnable(GL_DEPTH_TEST);

    emit ready();
}

void RenderArea::resizeGL(int w, int h)
{
    projection_.setToIdentity();
    auto ratio = w / (float)h;
    projection_.perspective(45.0f, ratio, 0.01f, 100.0f);

    camera.setViewport(w, h);
}

void RenderArea::paintGL()
{
    makeCurrent();

    QVector2D vf(width(), height());

    sp_.bind();

    glUniform2fv(sp_.uniformLocation("resolution"), 1, (GLfloat*)&vf);

    QMatrix4x4 model = model_2 * model_;
    QMatrix4x4 view = camera.matrix();
    QMatrix4x4 &projection = projection_;

    glUniformMatrix4fv(sp_.uniformLocation("projection"), 1, GL_FALSE, projection.data());
    glUniformMatrix4fv(sp_.uniformLocation("view"), 1, GL_FALSE, view.data());
    glUniformMatrix4fv(sp_.uniformLocation("model"), 1, GL_FALSE, model.data());

    glUniform1i(sp_.uniformLocation("widen"), 0);
    QOpenGLFramebufferObject fbo(size());
    fbo.bind();
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    selectionMode_ = true;
    drawScene();
    QImage im = fbo.toImage();
    QRgb id = im.pixel(cursor_.x(), cursor_.y());
    fbo.release();

    glUniform1i(sp_.uniformLocation("widen"), 1);
    GLuint tid = fbo.texture();

    QOpenGLFramebufferObject fbo2(size());
    fbo2.bind();

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, tid);
    glUniform1i(sp_.uniformLocation("qt_Texture0"), 0);


    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // ---------------------------------------------------------------------------------------------

    static QMatrix4x4 e;
    glUniformMatrix4fv(sp_.uniformLocation("projection"), 1, GL_FALSE, e.data());
    glUniformMatrix4fv(sp_.uniformLocation("view"), 1, GL_FALSE, e.data());
    glUniformMatrix4fv(sp_.uniformLocation("model"), 1, GL_FALSE, e.data());
    displace(e);

    QOpenGLBuffer vbo_(QOpenGLBuffer::VertexBuffer);
    QOpenGLBuffer ebo_(QOpenGLBuffer::IndexBuffer);
    QOpenGLVertexArrayObject vao_;

    vbo_.create();
    vao_.create();
    ebo_.create();

    vao_.bind();

    static GLfloat vertices[] = {
         1.0f,  1.0f, 0.0f,   1.0f, 1.0f,
         1.0f, -1.0f, 0.0f,   1.0f, 0.0f,
        -1.0f, -1.0f, 0.0f,   0.0f, 0.0f,
        -1.0f,  1.0f, 0.0f,   0.0f, 1.0f
    };
    static GLuint indices[] = {
        0, 1, 3,
        1, 2, 3
    };

    vbo_.bind();
    vbo_.allocate(vertices, sizeof(vertices));

    ebo_.bind();
    ebo_.allocate(indices, sizeof(indices));

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * 5, 0);
    glEnableVertexAttribArray(0);

    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * 5, (GLvoid*)(sizeof(GLfloat) * 3));
    glEnableVertexAttribArray(0);

    vbo_.release();
    vao_.release();

    vao_.bind();
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
    vao_.release();

    vao_.destroy();
    vbo_.destroy();
    ebo_.destroy();
    // ---------------------------------------------------------------------------------------------


    im = fbo2.toImage();
    id = im.pixel(cursor_.x(), cursor_.y());
    emit rendered(im);
    fbo2.release();

    highlightset_.clear();
    id = id & 0xffffff;
    if (id && id < 0xffffff)
    {
        highlightset_.insert(id);
    }

    glUniformMatrix4fv(sp_.uniformLocation("projection"), 1, GL_FALSE, projection.data());
    glUniformMatrix4fv(sp_.uniformLocation("view"), 1, GL_FALSE, view.data());
    glUniformMatrix4fv(sp_.uniformLocation("model"), 1, GL_FALSE, model.data());
    glClear(GL_COLOR_BUFFER_BIT);
    selectionMode_ = false;
    drawScene();
}

inline bool isRotation(QMouseEvent *e)
{
    return ((e->buttons() & Qt::MiddleButton) || (e->button() & Qt::MiddleButton))
            && !(e->modifiers() & Qt::ShiftModifier);
}

inline bool isPanning(QMouseEvent *e)
{
    return ((e->buttons() & Qt::MiddleButton) || (e->button() & Qt::MiddleButton))
            && (e->modifiers() & Qt::ShiftModifier);
}

void RenderArea::mousePressEvent(QMouseEvent *e)
{
    if (isRotation(e))
    {
        camera.start(e->pos());
        drag_ = true;
        cursor_ = e->pos();
    }
    else
    {
        if (highlightset_.size())
        {
            selectionset_.clear();
            selectionset_.insert(*highlightset_.begin());
            emit selected(selectionset_, document_);

            if (document_->jig())
            {
                document_->jig()->mouseDown(e, this);
            }
            else
            {
                document_->startJig(*highlightset_.begin());
            }
        }
    }

    update();
}

void RenderArea::mouseReleaseEvent(QMouseEvent *e)
{
    if (drag_)
    {
        cursor_ = e->pos();
        drag_ = false;
        model_ = model_2 * model_;
        model_2.setToIdentity();

        camera.update();
    }

    if (document_->jig() && !isRotation(e) && !isPanning(e))
    {
        document_->jig()->mouseUp(cursorToWorld(e->pos()), this);
    }

    update();
}

void RenderArea::mouseDoubleClickEvent(QMouseEvent *e)
{
    QOpenGLWidget::mouseDoubleClickEvent(e);
}

void RenderArea::mouseMoveEvent(QMouseEvent *e)
{
    QPoint shift = e->pos() - cursor_;

    if (isRotation(e))
    {
        camera.end(e->pos());
        e->accept();
    }
    else if (isPanning(e))
    {
        camera.pan(shift.x(), -shift.y());
        e->accept();
        cursor_ = e->pos();
    }
    else
    {
        cursor_ = e->pos();
        if (document_->jig())
        {
            document_->jig()->mouseMove(e, this);
        }
    }

    update();
}

void RenderArea::wheelEvent(QWheelEvent *e)
{
    camera.zoom(e->angleDelta().y() > 0 ? 1 : -1);
    e->accept();
    update();
}

void RenderArea::keyPressEvent(QKeyEvent *e)
{
    switch (e->key()) {
    case Qt::Key_Escape:
        if (document_)
        {
            document_->stopJig();
        }

        selectionset_.clear();
        emit selected(selectionset_, document_);
        break;
    case Qt::Key_Delete:
        if (document_ && selectionset_.size())
        {
            document_->removeItem(*selectionset_.begin());
            selectionset_.clear();
            emit selected(selectionset_, document_);
            update();
        }
    default:
        break;
    }

    if (document_ && document_->jig() != nullptr)
    {
        document_->jig()->keyPressed((Qt::Key)e->key());
    }
}

QVector3D RenderArea::cursorToWorld(const QPoint &screenPoint)
{
    // get cursor's world coordinates corresponding XOZ plane,
    // when y = 0 (1st coord)
    return cursorToWorld(1, screenPoint);
}

QVector3D RenderArea::cursorToWorld(int dim, const QPoint &screenPoint)
{
    QVector3D a(screenPoint.x() / (float)width() - 0.5,
                0.5 - screenPoint.y() / (float)height(),
                0.0f);
    a *= 2;
    auto b = a;
    a.setZ(1.0);
    b.setZ(0.0);

    auto m = projection_ * camera.matrix() * model_;

    auto inv = m.inverted();

    auto a0 = inv * a,
            a1 = inv * b;
    auto d = a1 - a0;

    float t = d.length() * a0[dim] / (a0[dim] - a1[dim]);
    d.normalize();
    auto pp = a0 + t * d;
    return pp;
}

// TODO(ET): make one method ^
QVector3D RenderArea::cursorToWorld(const QMatrix4x4 &local, const QPoint &screenPoint)
{
    QVector3D a(screenPoint.x() / (float)width() - 0.5,
                0.5 - screenPoint.y() / (float)height(),
                0.0f);
    a *= 2;
    auto b = a;
    a.setZ(1.0);
    b.setZ(0.0);

    auto m = projection_ * camera.matrix() * model_ * local;

    auto inv = m.inverted();

    auto a0 = inv * a,
            a1 = inv * b;
    auto d = a1 - a0;

    int dim = 0;
    float t = d.length() * a0[dim] / (a0[dim] - a1[dim]);
    d.normalize();
    auto pp = a0 + t * d;
    return pp;
}

void RenderArea::drawObject(Drawable *object)
{
    Q_ASSERT(object);

    if (selectionMode_)
    {
        glUniform1i(sp_.uniformLocation("object_id"), object->id());
    }
    else
    {
        glUniform1i(sp_.uniformLocation("object_id"), 0);
        auto color = highlightset_.contains(object->id()) ?
                         QVector3D() : object->color();
        setObjectColor(color);
    }

    object->draw(this);
}

const QSet<quint32> &RenderArea::selectionset()
{
    return selectionset_;
}

void RenderArea::drawScene()
{
    if (!document_)
    {
        return;
    }

    const QVector<DrawablePtr> &objects = document_->items();
    for (int i = 0; i < objects.size(); ++i)
    {
        if (objects[i])
        {
            displace(objects[i]->placement());
            drawObject(objects[i].data());
        }
    }

    for (int i = 0; i < objects_.size(); ++i)
    {
        if (objects_[i])
        {
            displace(objects_[i]->placement());
            drawObject(objects_[i].data());
        }
    }

    if (document_->jig())
    {
        document_->jig()->draw(this);
    }
}

void RenderArea::cleanUp()
{
    doneCurrent();

    objects_.clear();
    document_.reset();
}
