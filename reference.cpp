#include "reference.h"
#include "irenderarea.h"

Reference::Reference(QObject *parent) :
    Drawable(parent)
{
    // Y
    axes_[0] = FrustumPtr::create();
    axes_[0]->setPlacement(
                QMatrix4x4(
                    0,  1,  0,  0,
                    0,  0,  1,  0,
                    1,  0,  0,  0,
                    0,  0,  0,  1));
    axes_[0]->setColor(QColor(Qt::red));

    // X
    axes_[1] = FrustumPtr::create();
    axes_[1]->setPlacement(
                QMatrix4x4(
                    0,  0,  1,  0,
                    1,  0,  0,  0,
                    0,  1,  0,  0,
                    0,  0,  0,  1));
    axes_[1]->setColor(QColor(Qt::green));

    // Z
    axes_[2] = FrustumPtr::create();
    axes_[2]->setPlacement(
                QMatrix4x4(
                    1,  0,  0,  0,
                    0,  1,  0,  0,
                    0,  0,  1,  0,
                    0,  0,  0,  1));
    axes_[2]->setColor(QColor(Qt::blue));
}

void Reference::internalDrawChildren(IRenderArea *f)
{
    for (int i = 0; i < 3; ++i)
    {
        QMatrix4x4 place = placement() * axes_[i]->placement();
        f->displace(place);
        f->glDisable(GL_DEPTH_TEST);
        f->drawObject(axes_[i].data());
        f->glEnable(GL_DEPTH_TEST);
    }
}

void Reference::setupVertexAttributePointers(IRenderArea *)
{
}


