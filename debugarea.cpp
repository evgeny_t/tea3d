#include "debugarea.h"

#include <QPainter>

DebugArea::DebugArea(QWidget *parent) : QWidget(parent)
{
}

void DebugArea::updateArea(QImage im)
{
    image_ = im;
    update();
}

void DebugArea::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
    painter.drawImage(rect(), image_);
}
